package com.twuc.webApp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ControllerAdvice
public class ExceptionController {

    @GetMapping("/api/errors/illegal-argument")
    public ResponseEntity<String> handleException(){
        throw new RuntimeException();
    }

    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<ErrorMessage> handle(RuntimeException exception) {
        return ResponseEntity.status(404).body(new ErrorMessage("Something wrong with the argument"));
    }

    // 2.3
    @GetMapping("/api/error/null-pointer")
    public ResponseEntity<String> handleSameException(){
        throw new NullPointerException();
    }
    @GetMapping("/api/error/arithmetic")
    public ResponseEntity<String> handleOneException(){
        throw new NullPointerException();
    }

    @ExceptionHandler({NullPointerException.class, ArithmeticException.class})
    public ResponseEntity<ErrorMessage> handleTheDifferentException() {
        return ResponseEntity.status(418).body(new ErrorMessage("Something thing with the argument"));
    }

    // 2.4
    @GetMapping("/api/brother-errors/illegal-argument")
    public ResponseEntity<String> throwAnotherException(){
        throw new IllegalArgumentException();
    }

    @ExceptionHandler
    public ResponseEntity<ErrorMessage> handleException(IllegalArgumentException exception){
        return ResponseEntity.status(418).body(new ErrorMessage("Something wrong with brother or sister"));
    }
}
