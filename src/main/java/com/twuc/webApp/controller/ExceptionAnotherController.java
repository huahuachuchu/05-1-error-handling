package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class ExceptionAnotherController {
    @GetMapping("/api/sister-errors/illegal-argument")
    public void throwException(){
        throw new IllegalArgumentException();
    }
}
