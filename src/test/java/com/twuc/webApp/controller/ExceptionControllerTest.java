package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class ExceptionControllerTest {

    @Autowired
    private TestRestTemplate template;

    // 2.2
    @Test
    void should_handle_exception() {
        ResponseEntity<ErrorMessage> responseEntity = template
                .getForEntity("/api/errors/illegal-argument", ErrorMessage.class);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertEquals("Something wrong with the argument", responseEntity.getBody().getMessage());
    }

    // 2.3
    @Test
    void should_check_different_exception_using_same_method(){
        ResponseEntity<ErrorMessage> responseEntity = template
                .getForEntity("/api/error/null-pointer", ErrorMessage.class);
        assertEquals(HttpStatus.valueOf(418), responseEntity.getStatusCode());
        assertEquals("Something thing with the argument", responseEntity.getBody().getMessage());
    }

    @Test
    void should_verify_exception_using_one_method(){
        ResponseEntity<ErrorMessage> responseEntity = template
                .getForEntity("/api/error/arithmetic", ErrorMessage.class);
        assertEquals(HttpStatus.valueOf(418), responseEntity.getStatusCode());
        assertEquals("Something thing with the argument", responseEntity.getBody().getMessage());
    }

    //2.4
    @Test
    void should_check_same_exception_in_one_package(){
        ResponseEntity<ErrorMessage> responseEntity = template
                .getForEntity("/api/brother-errors/illegal-argument", ErrorMessage.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, responseEntity.getStatusCode());
        assertEquals("Something wrong with brother or sister", responseEntity.getBody().getMessage());
    }

    @Test
    void should_check_same_exception(){
        ResponseEntity<ErrorMessage> responseEntity = template
                .getForEntity("/api/sister-errors/illegal-argument", ErrorMessage.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, responseEntity.getStatusCode());
        assertEquals("Something wrong with brother or sister", responseEntity.getBody().getMessage());
    }
}
